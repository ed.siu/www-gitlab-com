(function() {
  //TweenMax.staggerTo('.hero-item', 1.6, {ease: Power4.easeOut, y: 0, opacity: 1}, 0.1);
  var videoThumb = document.getElementById('hero-video-thumb');
  var nav = document.getElementById('main-nav');
  var transparent = false;

  var heroVideoContainer = document.getElementById('hero-video-container');
  var heroVideo = document.getElementById('hero-video');
  var videoSource = 'https://www.youtube.com/embed/yjxrBSllNGo?autoplay=1&rel=0';

  // functions to show and hide the explainer video
  function showVideo() {
    heroVideoContainer.style.display = 'flex';
    heroVideo.src = videoSource;
  }

  function hideVideo() {
    heroVideoContainer.style.display = 'none';
    heroVideo.src = '';
  }

  function stopProp(event) {
    event.stopPropagation();
  }

  // scroll event to add and remove the header classes
  function changeNavClass() {
    if (nav.classList.contains('navbar-header-transparent')) {
      transparent = true;
    } else {
      // do nothing
    }
    if (window.scrollY >= 1) {
      // remove navbar-header-dark class
      nav.classList.remove('navbar-header-dark');
      nav.classList.remove('navbar-header-transparent');
      nav.classList.remove('navbar-header-transparent--light-purple');
    } else {
      // add it back
      if (transparent) {
        nav.classList.add('navbar-header-transparent');
        nav.classList.add('navbar-header-transparent--light-purple');
      } else {
        nav.classList.add('navbar-header-dark');
      }
    }
  }

  // only give the window a scroll function if the header is dark or transparent
  if (nav.classList.contains('navbar-header-dark') || nav.classList.contains('navbar-header-transparent')) {
    window.addEventListener('scroll', changeNavClass);
  }

  //videoThumb.addEventListener('click', showVideo);
  //heroVideoContainer.addEventListener('click', hideVideo);
  //heroVideo.addEventListener('click', stopProp);
})();
