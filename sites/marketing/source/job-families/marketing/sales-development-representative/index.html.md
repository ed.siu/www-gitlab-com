---
layout: job_family_page
title: "Sales Development Representative"
---

<iframe width="560" height="315" src="https://www.youtube.com/watch?v=eHzmPIL-TB0&feature=youtu.be" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br> 

## Job Description
GitLab is looking for an enthusiastic and strategic Sales Development Representative (SDR), to join our growing Revenue Marketing team. As a SDR at GitLab, you get the opportunity to lead the initial outreach to targeted (Commercial or Enterprise) accounts working in conjunction with the Field and Digital Marketing teams.
In this role you will leverage creative marketing and sales tactics to prospect and engage with multiple buyer personas and roles to introduce GitLab’s value. You will be responsible for generating qualified meetings and pipeline for the GitLab’s Sales organization.

We have an extensive onboarding and training program at GitLab and you will be provided with necessary DevOps and GitLab knowledge to fulfill your role.

### Responsibilities
* Effectively manage inbound lead flow as well as executing outbound prospecting initiatives
* Conduct high-level discovery conversations in target accounts
* Meet or exceed SDR sourced Sales Accepted Opportunity (SAO) volume targets
* Collaborate with and leverage teammates to develop targeted lists, call strategies, and messaging to drive opportunities
* Utilize business and industry knowledge to research accounts, identify key players, generate interest, create/identify compelling events, and develop accounts
* Work to have a variety of touches (call, email, social, etc.) on all leads in your assigned territory using Outreach.io
* Manage, track, and report on all activities and results using Salesforce
* Participate in documenting all processes in the GitLab handbook and update as needed with your Sales Development Manager
* Work in collaboration with Field and Corporate Marketing to drive attendance at regional marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts
* We’re a diverse team and we’re looking for you to bring your own unique flavor to the SDR role!

### Requirements

* Excited by the prospect of working cross-functionally with sales and different marketing departments. You'll have exposure to different departments like Sales, Marketing, Finance, Recruiting, Enablement, Engineering, etc. which will help you determine your career path at GitLab.
* Positive and energetic phone skills, excellent listening skills, strong writing skills
* A self-starter with a track record of successful, credible achievements
* You share our values, and work in accordance with those values
* Knowledge of business process, roles, and organizational structure
* Determined personality with a desire to grow and win
* Passionate about being a part of GitLab’s journey
* Proficient in using Salesforce and LinkedIn
* 2+ years work experience in a professional environment
* Previous tech industry experience or experience in sales development, marketing and/or sales is a plus
* Outbound prospecting experience is a plus
* Globally we require excellent written and spoken English which is our company language
* If in EMEA, fluency in spoken and written German or French or other European languages will be an advantage
* If in LATAM, fluency in Portuguese and Spanish is required
* Ability to use GitLab


## Levels

### Level I

*  **Learning and Development:** Complete onboarding issues and pass SDR Technical Development Training Level I in your first 180 days
*  **Performance:** The SDR must achieve 80% attainment for their total quota during months 1-3.  

*  **Example**
     * 	Month 1 quota 0
     * 	Month 2 quota 6
     * 	Month 3 quota 12
     * 	Total onboarding quota = 18, 80% performance expectation = 14.4, rounded to 14. SDR must achieve a minimum attainment of 14 to remain in their role.  

Upon successful completion of SDR Onboarding the SDR will be SDR Level I.

#### Job Grade

The SDR 1 is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Level II

*  **Learning and Development:** Pass SDR Technical Development Training Level I and Training Level II
*  **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level I. This does not include ramped months.

#### Job Grade

The SDR 2 is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Level III

* **Complete requirements for previous SDR levels**
* **Learning and Development:** Pass SDR Technical Development Training Level I, Training Level II  and Training Level III
* **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level II. This does not include ramped months.

#### Job Grade

The SDR 3 is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Team Lead
* Complete requirements for previous SDR levels
* **Learning and Development:** Pass SDR Technical Development Training Level I and Training Level II Training Level III
* **Performance:** Consecutively meet or exceed quotas for two quarters after achieving SDR Level III. This does not include ramped months.
* The Team Lead role is based on business need and will be posted if/when there is a need.

### SDR Promotion Process  
When an SDR qualifies for a promotion, they need to make a copy of the [SDR Promotion Template](https://drive.google.com/drive/search?q=%22SDR%20Promotion%20Doc%20Template%22), complete the doc, and share with their Manager. The SDR Manager will confirm that all areas are complete before submitting the promotion in Bamboo.  

**Required fields in the SDR Promotion Template: 
* Quarterly attainment details 
* List of [Tanuki Tech](/handbook/marketing/revenue-marketing/sdr/tanuki-tech/) courses required for promotion level 
* Examples of the SDR living [GitLab Values](/handbook/values#credit)

### SDR Performance Expectations
After one quarter of performing at a combined attainment of 80% or less of goal the SDR will receive a written warning of their performance. If in month 4 the SDR does not meet or exceed 80% of their combined goal they will go on a 30-day performance management plan. Performance management interventions are implemented by the SDR’s direct manager. Attainment is not the only reason for a performance improvement plan, additional information about GitLab code of conduct can be found [here](/handbook/people-group/code-of-conduct/) and how GitLab manages underperformance is detailed [here](/handbook/leadership/underperformance/).

#### Job Grade

The SDR Team Lead is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### SDR, Acceleration

As a member of the Sales Development Representative (SDR), Acceleration Team, you will work closely with the Field & Digital Marketing teams to accelerate prospect time-to-value. Proven outbound SDR or marketing experience is a must. You will also need to be well-versed with the DevOps conversations, GitLab value drivers, and account-based marketing (ABM) strategies. In addition to thinking creatively, you’ll need to have exceptional organizational and time management skills. Acceleration SDRs will be responsible for executing while planning and maintaining a roadmap for future campaigns.

#### Job Grade

The SDR, Acceleration is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

* Works with the GitLab field, digital, and product marketing teams to create targeted regional campaigns
* Creates outbound campaign content based on the Command of Message (CoM) Framework and GitLab Value Drivers
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately
* Builds Account & Prospect Target Lists based on our ideal customer profile attributes
* Executes targeted campaigns in specified regions
* Schedule qualified meetings for Regional Sales and Sales Development Teams

### Requirements

* 24 months of sales, outbound SDR, or related experience
* Proficient with the CoM framework, GitLab value drivers, and SDR tools
* Understanding of Account-based (ABM), Digital, and Field Marketing concepts
* Exceptional time management and organizational skills
* “Always-on” customer focus

## Specialties

### SDR (Public Sector)

#### SDR (Public Sector) Responsibilities

* Extends that of the SDR
* Focus on Public Sector customers

#### SDR (Public Sector) Requirements

* Extends that of the SDR
* previous experience working with Public Sector based customers or teams

### Potential Career Path

Subject to business need, GitLab team members from the Sales Development Representative job family typically interview for roles such as: [Manager, Sales Development](https://about.gitlab.com/job-families/marketing/sales-development-manager/), [Inside Sales Representative](https://about.gitlab.com/job-families/sales/public-sector-inside-account-representative/), and [Account Executive, SMB](https://about.gitlab.com/handbook/sales/commercial/smb/). All of these roles require a formal application and interview process, and are not considered a given promotion.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will then be invited to schedule an interview with a regional leader
* Following successful interviews, candidates will then be invited to complete a final writing assessment
* Successful candidates will subsequently be made an offer via phone or video

Additional details about our process can be found on our [hiring page](/handbook/hiring).
