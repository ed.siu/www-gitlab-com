---
layout: handbook-page-toc
title: "GitLab AnswerBase"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[GitLab Handbook](/handbook)>[GitLab Handbook: Engineering](/handbook/engineering)>[GitLab Handbook: Security Department](/handbook/engineering/security)>[GitLab Handbook: Security Assurance](/handbook/engineering/security/#assure-the-customer---the-security-assurance-sub-department)>[GitLab Handbook: Field Security](/handbook/engineering/security/security-assurance/risk-field-security)

## GitLab AnswerBase

**GitLab AnswerBase** is an internal only system of curated security and privacy relevant question and answer pairs maintained by the Risk and Field Security Team. This tool is designed to increase speed and efficiencies when completing [Customer Security Assessments](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html).

### Users and Responsibilities
There are three main types of users:
* **GitLab Team Members**- Individuals interested in or looking for answers to questions. This will mainly be Solutions Architects, Technical Account Managers, and/or Account Executives. 
* **Subject Matter Experts**- Individuals who are either stakeholders in the development of an answer or are the Control Owner directly linked to the question. This could be the Data Protection Officer, Infrastructure Team,  Security Specialty Teams, etc.  
* **Owners/Maintainers** - Individuals responsible for operation the system including triage of new questions, proposal of answers, coordination with SME users, and long term accuracy. This is the repsonsibility of the Risk and Field Security Team.

### Access and Instructions

[**GitLab AnswerBase**](https://gitlab.com/gitlab-private/sec-compliance/field_security/security-standard-answers-rfp/-/issues?scope=all&utf8=%E2%9C%93&state=closed) is accessible to GitLab Team members only at this time. [Detailed Instructions](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/procedures/gitlab-answerbase.html) including how to search for and pose new questions are maintained by the Risk and Field Security team. 

## Contact the Field Security Team
* Email
   * `fieldsecurity@gitlab.com`
* Slack
   * Feel free to tag is with `@field-security`
   * The `#sec-fieldsecurity`, `#sec-assurance`, `#security-department` slack channels are the best place for questions relating to our team (please add the above tag)
* [GitLab field security project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/risk-field-security)
