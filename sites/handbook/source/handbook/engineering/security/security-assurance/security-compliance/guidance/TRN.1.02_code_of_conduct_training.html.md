---
layout: handbook-page-toc
title: "TRN.1.02 - Code of Conduct Training Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TRN.1.02 - Code of Conduct Training

## Control Statement

All GitLab team-members sign a code of business conduct acknowledgement at the time of hire and annually thereafter.

## Context

The aim of this control is help ensure that all GitLab team-members are aligned on the values of the organization. The purpose of this alignment is to demonstrate to any external auditors that we hold all GitLab team-members to this same standard of conduct.

## Scope

This control applies to all GitLab team-members and contractors.

## Ownership

Control owner: 
* `People Operations`

Process owner: 
* People Operations

## Guidance

People Ops are responsible for deploying the process to ensure every GitLab team member has provided their signed acknowledgement of the code of conduct in the current year. All GitLab team members are responsible for reviewing and signing the GitLab Business Ethics and Code of Conduct.

## Additional control information and project tracking

Team members are required to review the GitLab Business Ethics and Code of Conduct and upload their signed acknowledgment upon completion at the time of hire and annually thereafter. The Code of Conduct is linked from the onboarding issue template as part of the new hire tasks. The [2020 Code of Conduct Training](https://gitlab.com/gitlab-com/people-group/General/issues/591#problem-or-issue-statement) has been rolled out. Legal and Security is consulted for the content. 

For audit evidence of compliance, we need to be able to demonstrate that all team members have signed the Business Ethics and Code of Conduct acknowledgement.

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Code of Conduct Training control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/932).

### Policy Reference

* Handbook policy [GitLab Business Ethics and Code of Conduct](/handbook/people-group/code-of-conduct/)
* [Acknowledgement form template](/handbook/people-group/code-of-conduct/#code-of-business-conduct--ethics-acknowledgment-form)

## Framework Mapping


* SOC2 CC
  * CC1.1
  * CC1.4
  * CC1.5
* PCI
  * 12.3
  * 12.3.5
