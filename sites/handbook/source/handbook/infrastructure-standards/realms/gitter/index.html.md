---
layout: handbook-page-toc
title: "Gitter Realm"
description: This is a placeholder page for the gitter realm.
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Quick links

* [Global infrastructure standards](/handbook/infrastructure-standards)
* [Global labels and tags](/handbook/infrastructure-standards/labels-tags)
* [Infrastructure policies](/handbook/infrastructure-standards/policies)
* [Infrastructure helpdesk](/handbook/infrastructure-standards/helpdesk)

## Overview

This is a placeholder page for the `gitter` realm.
