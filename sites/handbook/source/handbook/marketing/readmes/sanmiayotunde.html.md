---
layout: markdown_page
title: "Sanmi Ayotunde Ajanaku's README"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Sanmi (pronounced "Sun-Me") Ayotunde Ajanaku's README

Hi there! I'm **Sanmi**, my name is not that complicated, it's pronounced as **"sun-me"**, which means "Favored by God".  I am a Frontend Engineer at GitLab, and you will love working with me.

- [GitLab Handle](https://gitlab.com/sanmiayotunde)
- [Team Page](https://about.gitlab.com/company/team/#sanmiayotunde)
- [My Website](https://www.dabible.com/)
- [Twitter](https://twitter.com/sanmiayotunde)
- [Facebook](https://www.facebook.com/sanmiayotunde)
- [LinkedIn](https://www.linkedin.com/in/sanmiajanaku/)

I started my career as a graphics designer for my Dad's printing company at the age of 11. This developed my strange passion for computers and self-learning. I have spent all my life self-learning, even though I have a Bachelors in Computer Science, but my quest for knowledge and programming is unsatiable.

I've spent my career in and around web development, mobile app development, startups, advertising, strategy, people management, and customer journey transformation.  Having worked with my own startups, African based startups,  B2B and B2C companies in the United States, with manufacturing companies, Fortune 500s, and even small local owned businesses such as restaurants and other micro-businesses, I have developed a taste of understanding what users want online and how to give it to them easily and quickly.

## What's my current role and goal?

I develop web applications, components, contents, and anything you can click and interact with on web application ... I understand you want to know more, stay calm ...
* I will be updating this section soon. 😉
 

## About me

* I was born, raised, and studied in Nigeria, West Africa. I moved to the United States in 2013 after my Bachelor's Degree.
* In Nigeria, I was a performing musician until I had a turn around to become a Christian.
* In the United States, I have lived in Spokane. - Washington States,  Atlanta. - Georga, and Huntsville - Alabama. I have visited a lot of these places and approximately 30 states of the United States.
* I am committed to a lot of life-transforming activities for kids, young people, and others going through challenging faces in life.
* I currently have an NGO called [DaBible Foundation](https://www.dabible.com) where I provide mobile apps and solar-powered physical devices that people in African can use to listen to the Bible.
* I am presently engaged, and will be getting married soon ... You are invited to the party.
* I also grew up very poor (even though I never knew we were poor) in Africa, but I had a lot of huge dreams. Let me share a few...
	* Growing up, only rich folks can afford **milk**, one of my childhood dreams was to buy a tin of milk when I grow up, and drink everything alone! - Mission Accomplished
	* No one in our family had a car, so I had my **imaginary car** that I drive in my mind. Today, I have bought many cars. - Mission Accomplished!
	* **Airplanes**, I told my cousin I will be in one someday. He laughed like crazy and never believed me. - Mission Accomplished
	* I will **build a university** in my village - I still have no clue how this will happen, but it will surely happen lol
* I am a very interesting personality, I am still evolving, and I will be updating this section soon

## How you can help me

* Understand that we are all learning from each other, I may ask you silly questions.
* My silly questions lights up my imagination and helps me to understand your request better.
* I always love seeing the **big picture**, tell me the end goal, then tell me what you want us to do.
* Documentations! Documentations!! Documentations!!! - Please don't assume that I understand you. **A simple PowerPoint or Bullet Points** is worth more than a thousand Zoom calls lol.
* I always have a lot of tasks on my hand at the same time (prior experience). I may forget what you told me on the phone or zoom, that's why I prefer proper comment through **issues or slack** 

## My working style

 This section is currently culled from a colleague's profile but still fits my personality. Watch out for my updated version.
 
 - **I work in my issue boards**. Therefore, work doesn't exist to me without it being in an issue, assigned to a milestone. For one-off items, you'll find that I assign myself things in Google Docs from meetings. Please feel free to do the same, if we're not capturing our work in an issue. 
 - **I manage my to-do's for quick asynchronous exchanges**. This is usually where I see our conversation if we weren't actively working together on an issue or you've tagged me in a comment with an update. I clean to-do's out every week, and I am usually in them multiple times a day. 
 - **I see Slack as conversational and not real to our work**. If either of us has committed to doing something, we ought to get it into an issue and link there before the conversation is over.
 - **I rarely check emails**. Most takeaways in email need to be translated into a GitLab issue or 

## Contacting Me

Before you create an Issue, you are free to slack me, so that I can know the person behind the request. WE MAY BECOME THE BEST OF FRIENDS LOL :


I will be updating this page more and more.

Thanks!
