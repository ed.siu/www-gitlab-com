---
layout: handbook-page-toc
title: Learning & Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success and overall business strategy. We want to support the growth of our GitLab team-members' competencies, skills and knowledge by providing them with the tools they need and also the opportunities to progress their own personal and professional development.  

## How to Communicate with Us

Slack: `#learninganddevelopment`

Email Us: `learning@gitlab.com`


## Mission
Our mission is to provide resources to enable our team members to enhance success in their current roles as well as develop new skills to further their professional and personal development. We provide adaptive and blended learning and growth opportunities, including skills building, career development, and technical training that aligns with our strategic priorities as an organization. At GitLab, we are cultivating a culture of curiosity and continuous learning. 

## GitLab Learning Strategy & Roadmap

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRBdQFDSp8MBSkekUzM-DItdPxr-ETEjrCq85fhET_cC-6nAJGfwuFE-aK3jwSLJylg6yX8N1THaHsk/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Handbook First Training Content

All material in the handbook is considered training. The Learning & Development team pulls content from the handbook to build handbook first learning content. One of L&D's primary responsibilities is to ensure that content lives in the appropriate section in the handbook. In the below video, Sid, explains how the content of courses is not seperated from the handbook to the L&D team. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/G57Wtt7M_no" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## GitLab Learning & Development Principles

1. **Meaningful and relevant content.** We deliver learning solutions that drive the development and growth of team members throughout their life cycle at GitLab.

1. **Values aligned.** Our learning solutions reinforce GitLab’s [values](/handbook/values/), and foster continuous learning and curiosity.

1. **Diverse approaches to learning.** We apply a blended learning model for learning solutions, and adapt to various learning needs.

1. **Community.** We make our L&D offerings available to the public, aligned to our mission that everyone can contribute.

## Learning and Development Responsibilities

* Set learning strategy to develop and attract GitLab talent through a blend of immersive learning experiences
* Identifies and develops strategic relationships across the organization to motivate and develop team members
* Demostrate thought leadership and subject matter expertise in learning while applying adult learning theories
* Design learning solutions and experiences in support of organization values and culture, leadership principles, people manager core capabilities, career development, and more
* Deliver and develop training content to meet our strategic goals
* Perform learning needs analysis with leadership and e-group to understand and execute on learning and development opportunities

## L&D Organization

We are a small team but we've got a big role to play at GitLab! 

* [Learning and Development Partner ](/job-families/people-ops/learning-development-specialist/): [Josh Zimmerman](/company/team/#Josh_Zimmerman)
* [Learning and Development Generalist](/job-families/people-ops/learning-development-specialist/#learning--development-generalist): [Jacie Bandur](/company/team/#jbandur)
* [Learning and Development Associate](/job-families/people-ops/learning-development-specialist/#learning--development-associate): [Samantha Lee](/company/team/#slee24)

## Learning Sessions

### Live Learning
Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the schedule below as confirmed. If you were unable to attend a live learning session but still want to learn, check out our past live learning sessions below. At GitLab we [give agency](/handbook/values/#give-agency), but if you are attending Live Learning sessions you will be asked to be engaged and participate with your full attention.

<details>
  <summary markdown='span'>
    Format for Live Learning Sessions
  </summary>

<b>Format for 25 minute sessions:</b>

<ul>
<li>10 minutes - introduction/content</li> 
<li>10-15 minutes - Q&A</li>
</ul>

<b>Format for 50 minute sessions (times below are approximate):</b>

<ul>
<li>10-15 minutes - introduction/content</li>
<li>10-20 minutes - breakout session</li>
<li>10-20 minutes - debrief</li>
<li>5 minutes - conclusion</li>
</ul>

</details>

<details>
  <summary markdown='span'>
    Live Learning Schedule
  </summary>

<b>The upcoming Live Learning schedule is as follows:</b>

<ul>
<li>January - 3 Week Manager Challenge</li>
</ul>

</details>


<details>
  <summary markdown='span'>
    Past Live Learning Sessions
  </summary>

<b>2020</b>

<ul>
<li>January - <a href="https://youtu.be/crkPeOjkqTQ">Compensation Review: Manager Cycle (Compaas)</a></li>
<li>January - <a href="/company/culture/inclusion/being-an-ally/">Ally Training</a></li>
<li>February - <a href="/handbook/people-group/guidance-on-feedback/#receiving-feedback">Receiving Feedback</a></li>
<li>June - <a href="/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback">Delivering Feedback</a></li>
<li>June - <a href="/company/culture/inclusion/unconscious-bias/">Recognizing Bias</a></li>
<li>September - <a href="/handbook/people-group/learning-and-development/manager-challenge/#pilot-program">Manager Challenge Pilot</a></li>
<li>November - <a href="https://www.youtube.com/watch?v=WZun1ktIQiw">Belonging</a></li>
<li>November - <a href="/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge">One Week Challenge - Psychological Safety</a></li>
<li>December - <a href="/handbook/leadership/coaching/#introduction-to-coaching-1">Introduction to Coaching</a></li>
</ul>

<b>2019</b>

<ul>
<li>November - <a href="/company/culture/all-remote/effective-communication/">Communicating Effectively & Responsibly Through Text</a></li>
<li>December - <a href="/company/culture/inclusion/being-inclusive/">Inclusion Training</a></li>
</ul>

</details>


### Action Learning
[Action Learning](https://wial.org/action-learning/) sessions are designed to give team members a place to practice coaching skills by helping others work through specific challenges. 

<details>
  <summary markdown='span'>
    Format for Action Learning Sessions
  </summary>

<b>Format for 25 minute sessions:</b>

<ul>
<li>3 minutes - introduction</li>
<li>20 minutes - open up for attendees to present a current challenge they are facing. [Note: Other participants ask open ended questions about the challenge. No leading questions or advise is to be given. Once the person with the challenge feels they have received enough coaching, the group works on another participant's challenge.</li>
<li>2 minutes - conclusion</li>
</ul>

</details>

### GitLab Mini and Extended Challenges

All of GitLab's Learning and Development programs use the [GitLab tool to facilitate learning](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1). One way we do that is with the use of daily challenges. Daily challenges are a proven strategy for implementing new habits for our team members. Daily challenges can cover a range of topics and they are intended to introduce behavior changes that will stick. In September of 2020, we launched a [Four Week Manager Challenge program](/handbook/people-group/learning-and-development/manager-challenge/) that consisted of daily challenges and live learning events to strengthen foundational management skills. In November of 2020, we launched a [One Week Challenge on Psychological Safety](/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge). 

**Sample structure of a challenge:**
- Length of challenge: 1 week (But can be for as long as 4 weeks)
- Sample challenge topic: Giving Feedback
- Daily challenges: 20-minute daily challenge (Monday through Friday)
- Live Learning: Optional live learning session at the end of the week to revisit the challenges covered
- Self-Reflection: At the end of the week, have participants complete a self-reflection exercise
- Certification/Badging: Option to award participants with a certification or badge

## Learning Initiatives 

The L&D team host and organize multiple [learning initiatives](/handbook/people-group/learning-and-development/learning-initiatives/) to engage the GitLab team in opportunities to learn. The team is consistantly iterating on and adding to these resources!

## Additional Learning Opportunities

### Certifications

We provide our team members with certifications to demonstrate their knowledge on specific topics. We have [outlined](/handbook/people-group/learning-and-development/certifications/) our current certifications as well as planned and upcoming certifications for the year.

### Career Development

Everyone's career development is different, but we have [outlined](/handbook/people-group/learning-and-development/career-development/) what it can look like at GitLab. Career development also includes our [GitLab coaching framework](/handbook/leadership/coaching/) to support managers with holding coaching discussions with their team. 

### Developing Emotional Intelligence

Whether you are a People Manager or an Individual Contributor, being skilled in "emotional intelligence" (also referred to as EQ) is a key attribute to interpersonal effectiveness. We have [outlined](/handbook/leadership/emotional-intelligence/) the definition of emotional intelligence, how to understand your own EQ, how to develop your EQ in a [remote setting](/company/culture/all-remote/guide/), and building an inclusive environment with EQ. 

Another strategy to improve emotional intelligence is to apply the [Social Styles framework](/handbook/leadership/emotional-intelligence/social-styles/) within your team to increase interpersonal interactions and team dynamics. 

### Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)

There is also a way to practice foreign languages and ask for advice in several Slack channels, each dedicated to a specific language. You can find all these channels by searching for channels starting with #lang. If you're missing a channel for your target language, feel free to create one and mention it in #whats-happening-at-gitlab so that fellow GitLab team-members can join too!

### Manager Enablement Programs

GitLab has a growing [resource](/handbook/people-group/learning-and-development/manager-development/) to enable all team members transitioning to a manager role. It contains a link to a checklist, readings, and a form to help learning and development customize your development as a manager.

We piloted the [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) program during the month of September to over 30 people leaders at GitLab. The Manager Challenge is to first step to building out a comprehensive manager enablement curriculum for People Leaders. We used daily challenges and the GitLab tool to facilitate async learning in a remote environment. This was the first program of its kind at GitLab and we look forward to rolling out more manager enablement initiatives in the future.  

## New Learning Content at GitLab

We are always working to create more learning content for our team members. If you have a learning request that you would like the Learning & Development team to develop in partnership with your team, please fill out a `learning-and-development-request` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). Our team will review and set the priority for your request based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

If you have developed the learning content and would like the Learning & Development team to review, fill out a `learning-and-development-review` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). 

Our team will review and set the priority for your content request or review based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

### How the L&D team prioritizes requests: 
*  Evaluate strategic impact of the learning session
*  Determine the level of work associated with the learning requirement
*  Assess the impacted audience groups of the session 
*  Identify measures of success 
*  Assess dates of delivery with course schedule and forecast future date

### L&D team sprints

The L&D team uses GitLab issue boards to track priorities, organize collaboration, and set due dates based on quartley sprints.

* The L&D team uses [this sprint issue board](hhttps://gitlab.com/groups/gitlab-com/people-group/learning-development/-/boards/1958538) to track priority issues each quarter.
* The `open` list is a queue for issues that need to be addressed, but have not yet been assigned to a sprint.
* The `L&D Ongoing` list is meant for long term projects the team is working on that cannot be accomplished in one sprint, but are a priority.
* Following the ongoing list, the team maintains three milestones, one for each upcoming sprint. The sprints are organized with a milestone using the naming mechanism `L&D Sprint # (Date Range)`
* Issues can be moved from the `open` list to the correct sprint when they are ready to be assigned/addressed.
* When an issue is closed, it should remain in the given milestone.
* At the end of the sprint, the milestone can be removed from the board, or the milestone list can be hidden on the issue board.
* The `L&D Requests` column should be used to organize requests for L&D support or courses coming from other teams.

Additional planning and notes around the sprint board can be found in [this issue.](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/111)

#### Best practices for using sprint boards and issues:

- Apply a burn-down chart with milestones to track time spent
- Create the issue in the sprint where the work starts
- When creating L&D contnet, apply one issue to one course when developing content. Maintain all course development material in the issue for organization. 
- Epics do not show up in the boards
- Apply a Storyboard template for course development
- Apply labels to manage different work and priorities (leadership requests, prioritized, p1-p3, triage, WIP, Backlog)
- Consider having GitLab team members vote on priority issues to determine need and interest in learning & development content
- Stack rank issues in the board based on priority if possible

### Top five training content development principles

If you are devleoping content to meet your learning needs or partnering with the L&D team, here are five key principles to consider when formulating a learning session: 

1. **Know Your Audience** - Analyze and assess the course audience. Ensure that all audience needs are accounted for at every level in the organization you are delivering the training too. 

2. **Define Learning Objectives** - Highlight what you want the learner to walk away from the session with. Consider developing two to three broad overall statements of what the audience will acheive. 

3. **Break Down Complex Information** - Consider breaking down complex information into easy to digest visuals or text. Reference the handbook but do not be afraid to create a visual representation or use storytelling for the audience.

4. **Engage the Learner** - Adults learn through practice and involvement. Consider using tools to engage learners in a virtual setting like [Mentimeter](https://www.mentimeter.com/) or [Kahoot](https://kahoot.com/business-u/) to stimulate interactivity. Ask the [L&D team](/handbook/people-group/learning-and-development/) for more insight on learning engagement tools. There are a lot you can leverage! 

5. **Implement Blended Learning Course Content** - Give the audience some pre-course work to read and review before the learning session. Use off-the-shelf resources and ensure the content is applicable to what will be covered in the session. Follow up with the audience following the session to gauge how they've applied what they've learned on the job through surveys and questionmaires. 

### Application of Adult Learning Theory

Adults learn differently in the workplace than they would in traditional learning environments or how they learned growing up. If you are developing training, consider applying principles related to Adult Learning Theories, those include: 

1. **Transformative learning:** The learning experience should aim to change the individual through transformative learning approaches. Start with learning experiences that appeal to your specific audience, and then move to activities that challenge assumptions and points of view.   

2. **Self-directed learning:** Most of the learning that adults do is outside the context of formal training, so there should be an emphasis on augmenting those informal learning experiences. Infuse applications of pre-reads and post-course follow up. Have the participants bring up examples of self-directed learning that they have taken that is related to the training course. 

3. **Experiential learning:** Adults learn through experiences and by doing. When designing a learning experience, apply activities to stimulate learning by doing through role-playing, simulations, virtual labs, case studies, etc. 

4. **Andragogy:** Recognize that adults learn differently than children. Design learning experiences with the assumption that your participants will come to the table with their own set of life experiences and motivations. Adults tend to direct their own learning, tend to learn better by doing, and will want to apply their learning to concrete situations as soon as possible. 

### Developing Learning Objectives

If you are developing training, add learning objectives to the beginning of the content to state a clear outcome of the training. A clear learning objective states what the learner will be able to do upon completion of a learning/training activity. Good learning objectives are what you want team members to learn or achieve. 

**Steps to creating learning objectives:** 
1. Identify the level of knowledge necessary to achieve the objective of the training. Use [Bloom's Taxonomy](https://tips.uark.edu/using-blooms-taxonomy/) to assist with writing effective learning objectives. 
2. Select an [action verb](https://www.bu.edu/cme/forms/RSS_forms/tips_for_writing_objectives.pdf). 
3. Create your very own objective
4. Check your objective. Make sure it includes these four pieces: audience, behavior, condition, and degree of mastery
5. Repeat these steps for each objective

**Sample learning objectives:** 
- By the end of the session, team members will be able to accurately describe the steps take to address underperformance
- Team members will be able to apply the GROW coaching model framework to coaching sessions with members of their team 
- After learning about the high-performance team-building model, team members will be able to determine the steps needed to reach high performance.

### 70-20-10 Model for Learning and Development

The L&D team uses a formula to describe the optimal sources of learning at GitLab. It shows that team members obtain 70 percent of the knowledge from job-related experiences, 20 percent from interactions with others, and 10 percent from formal learning events. The model is intended to show that hands-on experience (70 percent) can be one of the most beneficial for team members because it enables them to discover and refine their job-related skills. Team members learn from others (20 percent) through a variety of activities we offer every week. Lastly, the formula holds that only 10 percent of professional development comes from traditional live learning and eLearning events. 

### Instructional Design 101

The Learning & Development, [Field Enablement](/handbook/sales/field-operations/field-enablement/), and [Professional Services](/handbook/customer-success/professional-services-engineering/) team hosted an Instructional Design 101 session on 2020-10-28 to better understand concepts of adult learning theory and instructional design. Check out the video to learn how to apply instructional design to learning content. 

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Be8oRHp0E84" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


## [GitLab Learn](https://gitlab.edcast.com/): EdCast Learning Experience Platform (LXP) 

We are currently in the process of implementing GitLab Learn, our [EdCast Learning Experience Platform (LXP)](/handbook/people-group/learning-and-development/gitlab-learn.). **The LXP will launch internally for GitLab team members on 2021-01-11** This system is more than just a Learning Management System, it is a platform to help deliver engaging learning tailored to you and your role. It can direct to handbook pages, make recommendations for learning based on previous consumption, and serve interactive learning content. 

We are also in the process of launching LinkedIn Learning internally for all team members. Stay tuned for more details and the content will be supplemental to GitLab customized content and apply a [Handbook-first approach to interactive learning](/handbook/people-group/learning-and-development/interactive-learning/)

## Compliance Courses 

GitLab has a number of compliance courses and resources. Explore the drop downs below to learn more. 

<details>
  <summary markdown='span'>
    Common Ground: Harassment Prevention Training
  </summary>

All team members are required to take a harassment prevention training. All new team members will have a task in their onboarding issue to complete this training using <a href="https://learning.willinteractive.com/">Will Interactive's Platform</a> within the first 30 days of hire. Once you get to that step in your onboarding issue, please do the following:

<ol>
<li>Log into BambooHR</li>
<li>On the Training tab, click on the Harassment Prevention training that aligns with your role (Supervisor or Team Member) and location (U.S. or Non-U.S.). The list of courses to choose from are: <b>FY21 Anti-Harassment Training for Non-U.S. Team Members</b>, <b>FY21 Anti-Harassment Training for Non-U.S. Supervisors</b>, <b>FY21 Anti-Harassment Training for U.S. Team Members</b>, or <b>FY21 Anti-Harassment Training for U.S. Supervisors</b>. You only need to complete one training. For managers and leaders, the course is 2 hours long, but you can stop and come back to it. For all other GitLab Team Members, this is 1 hour long.</li>
<li>Click on the <i>Sign Up Now</i> link</li>
<li>Enter in your name and GitLab email address</li>
<li>Create a password</li>
<li>You may be sent a link to verify your account</li>
<li>Once you have logged in successfully you will be taken to the course you selected in BambooHR. (You can use the navigation bar at the top right-hand side of screen for volume and screen settings. To the left and right of the center screen you should see this symbol: > which you can click on to move forward and back through the training.)</li>
<li>Once completed, please upload a copy of your certificate in BambooHR in the <i>Employee Uploads</i> folder</li>
<li>You may also keep a record of the certificate for your own files. To create the certificate, click on <i>view</i> in the course title</li>
<li>Scroll down to <i>users</i> then click on <i>completion certificates</i> to download the PDFs</li>
</ol>

If a Team Member moves from an individual contributer role to a manager role, they will be assigned a New Manager Enablement issue to work through. In this issue it will be verified if they have completed the correct level of harassment prevention training. If the team member has completed the "Team Member" level of the training, they will have to take the "Supervisor" level of the training as well.

Our <a href="/handbook/anti-harassment/?private=1">Anti-Harassment Policy</a> outlines guidelines, reporting and disciplinary action.  

</details>

<details>
  <summary markdown='span'>
    Compliance Next
  </summary>


The Compliance Next community offers free membership and comes with tons of amazing resources and tools for compliance professionals as well as people just wanting to learn more about the compliance space. 

You can create a free account on the [Compliance Next](https://www.navexglobal.com/compliancenext/) website using your own credentials. 

One of the most unique and interesting aspects of Compliance Next is the ability to connect directly with experts and peers. The platform offers different types of educational resources that you can watch, read and use—all of which provide the ability to comment and ask questions to compliance professionals and other members of the community.

</details>

If you have any questions or need further help please ping people ops in the `#peopleops` channel in slack.

### Compliance Processing Steps for L&D Team

#### Checking Compliance for Offboarded Team Members

When tagged in an offboarding issue, follow these steps to check completion/license status of appropriate anti-harassment courses in the WILL Learning platform.

It is important this step is completed for each offboarding issue, as GitLab pays per license in WILL Learning. This compliance check insures that GitLab is aware of, and making use of, all paid licenses.

1. Navigate to the offboarding issue. Copy the user's email address.
1. Collect Start Date: Linked to the offboarding issue is a merge request to remove the team member from relevant pages. Review the MR change log to check the start date of the team member. This start date will indicate which WILL Learning course the team member would have completed.
1. Collect Location Information: On the same MR, make note of the team member's location.
1. Collect Management Information: On the offboarding issue, check for the individuals title at GitLab and determine if they were a manager of people. If you cannot tell from the offboarding issue, check their Slack profile or refer to the GitLab Org Chart.
1. Open the WILL Learning platform.
1. Based on the team member's location, management level, and start date, navigate to the WILL Learning course they would have been invited to take. For example, a team member who manages people, joined GitLab in 2019, and lives outside of the USA would have been invited to take the `2019 Anti-Harassment Training for Supervisors` course. Click the `MANAGE` button to enter the course.
1. Check for Completion: The following steps should be taken to determine the current status of the individual's WILL Learning license - either `Finished`, `In Progress`, or `Not Started`. Please note - if the user does not appear in the first WILL Learning course you check, attempt to look for the user in other approriate courses. Note that at this time, there is no function in the WILL Learning platform to search all courses at once.

     1. If a search for the user's email address returns a `Finished` status, check the de-provisioned box on the offboarding issue and comment on the issue `WILL Learning course finished, no license to revoke`
     1. If a search for the user's email address returns an `In Progress` status, check the de-provisioned box on the offboarding issue and comment on the issue `WILL Learning in progress, cannot revoke license`. Email our WILL Learning account reresentative to request a refund of the license.
     1. If a search for the user's email address returns a `Not Started` status, check the de-provisioned box on the offboarding issue and comment on the issue `WILL Learning in progress, cannot revoke license`. Email our WILL Learning account reresentative to request a refund of the license.
     1. If a search for the user's email address returns a no results, follow the next steps to revoke the license.

1. Revoke License: The following steps should be taken if you cannot find record of the course status for the offboarding team member.

     1. Within the appripriate WILL Learning course, click `Invite Participants`
     1. Under the `Pending Invitations` section, click `View All`
     1. Search the page using the team member's email address
     1. If the user's name appears in the `Pending Invitations` section, click the `revoke` link to remove their access to the license
     1. On the offboarding issue, check the de-provisioned box and comment on the issue `WILL Learning course not started, license revoked`

1. When you have completed this process, be sure to shut off notifications on the offboarding issue to avoid inbox noise.


#### Supporting New Managers of People at GitLab

When new team members join the GitLab team as managers of people, the L&D team reminds them to take the assigned WILL Learning anti-harassment course.

1. The employment bot will open a new issue for team members who are joining as managers of people
1. Comment on this issue and tag the new manager with a friendly reminder to take their anti-harassment WILL Learning course.
1. You can use the steps in the `Checking Compliance for Offboarded Team Members` section to determine if the new manager has completed the course
1. If the user has already completed the course, consider posting a friendly thanks on their employment issue.
1. Unfollow the employment issue to avoid future inbox noise.

#### Supporting Transitioning team members to Managers of People

When current team members transition to roles and become managers of people, the L&D team assigns a new anti-harassment course in WILL Learning for the team member to complete.

1. The employment bot will open a new issue for team members who are transitioning to managers of people
1. Find the user's location using their Slack or GitLab profile
1. In WILL Learning, navigate to the approripate training for supervisors based on this geographical area.
1. In the course, click `Invite Participants` and copy the sharable link
1. On the employment issue, comment using the following template: `Hello [NAME]! Please follow this link [COURSE LINK] to complete  the [COURSE NAME] in WILL Learning. Thanks!`
1. Follow the steps above in the `Checking Compliance for Offboarded Team Members` section to determine if this team member has completed the individual anti-harassment course in their previous role. If they did not complete the course, follow steps to revoke their license.


## Performance Indicators

- **Engagement Survey Growth and Development Score > X%**

Questions related to growth and development on the semi-annual [Engagement Survey](/handbook/people-group/engagement/) have a favorable score. The exact target is to be determined.

- **Rate of internal job promotions > X%**

Total number of [promotions](/handbook/people-group/promotions-transfers/) in a rolling six month period/total number of employees. The target for this is to be determined.

- **12 month voluntary team member turnover related to growth < X%**

This is calculated the same as [12 month voluntary team member turnover KPI](/handbook/people-group/people-group-metrics/#team-member-turnover) but is using the number of team members actively choosing to leave GitLab to growth and development related reasons only. The target is to be determined.

- **Exit Survey Data < 5 % of team members exiting GitLab related to L&D**

Questions related to Learning & Development opportunities at GitLab through [Exit Survey Data](https://docs.google.com/document/d/1kPmaQwAsTujbclFSH2J2Le-OAPT1BP5JitFyCqe1BO8/edit#)

